import {BrowserModule} from '@angular/platform-browser';
import {NgModule} from '@angular/core';

import {AppComponent} from './app.component';
import {RouterModule, Routes} from '@angular/router';
import {AboutUsComponent, ContactComponent, ServicesComponent, VisitorComponent, VisitorModule, WelcomeComponent} from './visitor';

const ROUTES: Routes = [
  {
    path: '',
    component: VisitorComponent,
    children: [
      {path: '', component: WelcomeComponent},
      {path: 'about-us', component: AboutUsComponent},
      {path: 'contact', component: ContactComponent},
      {path: 'services', component: ServicesComponent}
    ]
  },
  {path: 'auth', loadChildren: './auth/auth.module#AuthModule'},
  {path: 'dashboard', loadChildren: './dashboard/dashboard.module#DashboardModule'},
  {path: '**', redirectTo: ''}
];


@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    VisitorModule,
    RouterModule.forRoot(ROUTES)
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule {
}
