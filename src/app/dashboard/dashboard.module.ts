import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {DashboardComponent} from './dashboard.component';
import {RouterModule, Routes} from '@angular/router';
import {SharedModule} from './shared';

const ROUTES: Routes = [
  {
    path: '',
    component: DashboardComponent,
    children: [
      {path: 'events', loadChildren: './events/events.module#EventsModule'},
      {path: 'inventory', loadChildren: './inventory/inventory.module#InventoryModule'},
      {path: 'meeting', loadChildren: './meeting/meeting.module#MeetingModule'},
      {path: 'user-management', loadChildren: './user-management/user-management.module#UserManagementModule'}
    ]
  },
  {path: '**', redirectTo: ''}
];

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    RouterModule.forChild(ROUTES)
  ],
  declarations: [DashboardComponent],
  exports: [RouterModule]
})
export class DashboardModule {
}
