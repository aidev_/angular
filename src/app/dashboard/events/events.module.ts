import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {EventsComponent} from './events.component';
import {RouterModule, Routes} from '@angular/router';

const ROUTES: Routes = [
  {path: '', component: EventsComponent},
  {path: '**', redirectTo: ''}
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ROUTES)
  ],
  declarations: [EventsComponent]
})
export class EventsModule {
}
