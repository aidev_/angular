import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {MeetingComponent} from './meeting.component';
import {RouterModule, Routes} from '@angular/router';

const ROUTES: Routes = [
  {path: '', component: MeetingComponent},
  {path: '**', redirectTo: ''}
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ROUTES)
  ],
  declarations: [MeetingComponent]
})
export class MeetingModule {
}
