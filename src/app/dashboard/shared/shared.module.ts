import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {LayoutModule} from './layout';

@NgModule({
  imports: [
    CommonModule
  ],
  exports: [LayoutModule]
})
export class SharedModule {
}
