import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {InventoryComponent} from './inventory.component';
import {RouterModule, Routes} from '@angular/router';

const ROUTES: Routes = [
  {path: '', component: InventoryComponent},
  {path: '**', redirectTo: ''}
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ROUTES)
  ],
  declarations: [InventoryComponent]
})
export class InventoryModule { }
