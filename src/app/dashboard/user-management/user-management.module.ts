import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {UserManagementComponent} from './user-management.component';
import {RouterModule, Routes} from '@angular/router';

const ROUTES: Routes = [
  {path: '', component: UserManagementComponent},
  {path: '**', redirectTo: ''}
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(ROUTES)
  ],
  declarations: [UserManagementComponent]
})
export class UserManagementModule { }
