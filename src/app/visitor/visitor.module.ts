import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {WelcomeComponent} from './welcome/welcome.component';
import {AboutUsComponent} from './about-us/about-us.component';
import {ContactComponent} from './contact/contact.component';
import {ServicesComponent} from './services/services.component';
import {VisitorComponent} from './visitor.component';
import {RouterModule} from '@angular/router';
import {SharedModule} from './shared';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    SharedModule
  ],
  declarations: [WelcomeComponent, AboutUsComponent, ContactComponent, ServicesComponent, VisitorComponent],
  exports: [WelcomeComponent, AboutUsComponent, ContactComponent, ServicesComponent, VisitorComponent],
})
export class VisitorModule { }
